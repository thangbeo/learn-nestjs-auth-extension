import { ActiveUserData } from '@/iam/interfaces/active-user-data.interface';
import { PassportSerializer } from '@nestjs/passport';

export class UserSerializer extends PassportSerializer {
  serializeUser(user: any, done: (err: Error, user: ActiveUserData) => void) {
    done(null, {
      sub: user.id,
      email: user.email,
      role: user.role,
      permissions: user.permissions,
    });
  }

  async deserializeUser(
    payload: any,
    done: (err: Error, user: ActiveUserData) => void,
  ) {
    done(null, payload);
  }
}
