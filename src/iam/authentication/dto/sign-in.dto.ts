import {
  IsEmail,
  IsOptional,
  MinLength,
  IsNumberString,
} from 'class-validator';
export class SignInDto {
  @IsEmail()
  email: string;

  @MinLength(10)
  password: string;

  @IsOptional()
  @IsNumberString()
  tfaCode?: string;
}
